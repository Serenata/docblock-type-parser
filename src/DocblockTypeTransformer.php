<?php

namespace Serenata\DocblockTypeParser;

use Closure;

/**
 * Transforms a tree of {@see DocblockType} objects into a tree of new objects by applying a transformation on them.
 */
final class DocblockTypeTransformer implements DocblockTypeTransformerInterface
{
    /**
     * @inheritDoc
     */
    public function transform(DocblockType $docblockType, Closure $transformer): DocblockType
    {
        $transformedType = $transformer($docblockType);

        if ($transformedType instanceof CompoundDocblockType) {
            return new CompoundDocblockType(...array_map(function (DocblockType $type) use ($transformer) {
                return $this->transform($type, $transformer);
            }, $transformedType->getParts()));
        }

        return $transformedType;
    }
}
