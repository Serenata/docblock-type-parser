<?php

namespace Serenata\DocblockTypeParser;

/**
 * Represents a boolean as docblock type.
 *
 * {@inheritDoc}
 */
class BooleanDocblockType extends BoolDocblockType
{
    /**
     * @var string
     */
    public const STRING_VALUE = 'boolean';

    /**
     * @inheritDoc
     */
    public function toString(): string
    {
        return self::STRING_VALUE;
    }
}
