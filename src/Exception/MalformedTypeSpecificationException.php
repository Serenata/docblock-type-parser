<?php

namespace Serenata\DocblockTypeParser\Exception;

use LogicException;

/**
 * Indicates a malformed type was passed.
 */
class MalformedTypeSpecificationException extends LogicException
{

}
