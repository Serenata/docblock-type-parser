<?php

namespace Serenata\DocblockTypeParser;

/**
 * Represents an int as docblock type.
 *
 * {@inheritDoc}
 */
class IntDocblockType extends SpecialDocblockType
{
    /**
     * @var string
     */
    public const STRING_VALUE = 'int';

    /**
     * @inheritDoc
     */
    public function toString(): string
    {
        return self::STRING_VALUE;
    }
}
