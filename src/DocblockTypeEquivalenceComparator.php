<?php

namespace Serenata\DocblockTypeParser;

use DomainException;

/**
 * @inheritDoc
 */
final class DocblockTypeEquivalenceComparator implements DocblockTypeEquivalenceComparatorInterface
{
    /**
     * @inheritDoc
     */
    public function areEquivalent(DocblockType $a, DocblockType $b): bool
    {
        if ($a instanceof SpecialDocblockType) {
            return $this->isSpecialDocblockTypeEquivalentTo($a, $b);
        } elseif ($a instanceof ClassDocblockType) {
            return $this->isClassDocblockTypeEquivalentTo($a, $b);
        } elseif ($a instanceof CompoundDocblockType) {
            return $this->isCompoundDocblockTypeEquivalentTo($a, $b);
        }

        throw new DomainException('Don\'t know how to compare "' . get_class($a) . '" to "' . get_class($b) . '"');
    }

    /**
     * @param SpecialDocblockType $a
     * @param DocblockType        $b
     *
     * @return bool
     */
    private function isSpecialDocblockTypeEquivalentTo(SpecialDocblockType $a, DocblockType $b): bool
    {
        return get_class($a) === get_class($b);
    }

    /**
     * @param ClassDocblockType $a
     * @param DocblockType      $b
     *
     * @return bool
     */
    private function isClassDocblockTypeEquivalentTo(ClassDocblockType $a, DocblockType $b): bool
    {
        return $b instanceof ClassDocblockType && $b->getName() === $a->getName();
    }

    /**
     * @param CompoundDocblockType $a
     * @param DocblockType         $b
     *
     * @return bool
     */
    private function isCompoundDocblockTypeEquivalentTo(CompoundDocblockType $a, DocblockType $b): bool
    {
        return
            $b instanceof CompoundDocblockType &&
            $this->isCompoundDocblockTypeSubsetOf($a, $b) &&
            $this->isCompoundDocblockTypeSubsetOf($b, $a);
    }

    /**
     * @param CompoundDocblockType $a
     * @param CompoundDocblockType $b
     *
     * @return bool
     */
    private function isCompoundDocblockTypeSubsetOf(CompoundDocblockType $a, CompoundDocblockType $b): bool
    {
        foreach ($a->getParts() as $aPart) {
            $equivalentItemsInB = $b->filter(function (DocblockType $bPart) use ($aPart): bool {
                return $this->areEquivalent($aPart, $bPart);
            });

            if (empty($equivalentItemsInB)) {
                return false;
            }
        }

        return true;
    }
}
