<?php

namespace Serenata\DocblockTypeParser;

/**
 * Compares two {@see DocblockType} objects to see if they are equivalent.
 *
 * This merely checks if two docblock types denote the same types, which makes them "directly" semantically equivalent,
 * but not if they are semantically equivalent in that type specializations are taken in order. In other words, either
 * the type specification both types generat must be identical, or they must be identical save for the fact that the
 * ordering differs.
 *
 * @example "int|null" === "int|null"
 * @example "int|null" === "null|int"
 * @example "int[]|null" !== "array|int"
 */
interface DocblockTypeEquivalenceComparatorInterface
{
    /**
     * @param DocblockType $a
     * @param DocblockType $b
     *
     * @return bool
     */
    public function areEquivalent(DocblockType $a, DocblockType $b): bool;
}
