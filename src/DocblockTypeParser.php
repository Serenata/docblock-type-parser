<?php

namespace Serenata\DocblockTypeParser;

/**
 * @inheritDoc
 */
final class DocblockTypeParser implements DocblockTypeParserInterface
{
    /**
     * @var string
     */
    private const ARRAY_TYPE_HINT_REGEX = '/^(.+)\[\]$/';

    /**
     * @var string
     */
    private const COMPOUND_TYPE_SPLITTER = '|';

    /**
     * @inheritDoc
     */
    public function parse(string $specification): DocblockType
    {
        if ($specification === '') {
            throw new Exception\MalformedTypeSpecificationException('A non-empty type must be passed');
        }

        $specification = $this->getSanitizedSpecification($specification);

        if ($this->isCompoundTypeSpecification($specification)) {
            return $this->parseCompoundTypeSpecification($specification);
        }

        return $this->parseNonCompoundTypeSpecification($specification);
    }

    /**
     * Sanitizes the specification.
     *
     * The passed specification will be sanitized so that leading and trailing spaces are removed. Incorrect symbols
     * at the start and end of types will also be pruned.
     *
     * @param string $specification The specification to sanitize.
     *
     * @return string
     */
    private function getSanitizedSpecification(string $specification): string
    {
        $specification = trim($specification, ' ' . self::COMPOUND_TYPE_SPLITTER);

        if (!empty($specification) && $specification[0] === '(' && mb_substr($specification, -1) === ')') {
            $specification = mb_substr($specification, 1, -1);
        }

        $specification = trim($specification, ' ' . self::COMPOUND_TYPE_SPLITTER);

        while (true) {
            $specification = str_replace(
                self::COMPOUND_TYPE_SPLITTER . self::COMPOUND_TYPE_SPLITTER,
                self::COMPOUND_TYPE_SPLITTER,
                $specification,
                $count
            );

            if ($count === 0) {
                break;
            }
        }

        return $specification;
    }

    /**
     * @param string $specification
     *
     * @return bool
     */
    private function isCompoundTypeSpecification(string $specification): bool
    {
        $length = mb_strlen($specification);

        $parenthesesOpened = 0;
        $parenthesesClosed = 0;

        for ($i = 0; $i < $length; ++$i) {
            if ($specification[$i] === '(') {
                ++$parenthesesOpened;
            } elseif ($specification[$i] === ')') {
                ++$parenthesesClosed;
            }

            if ($parenthesesOpened === $parenthesesClosed && $specification[$i] === self::COMPOUND_TYPE_SPLITTER) {
                return true;
            }
        }

        return false;
    }

    /**
     * @param string $specification
     *
     * @return CompoundDocblockType
     */
    private function parseCompoundTypeSpecification(string $specification): CompoundDocblockType
    {
        $parts = [];

        $start = 0;
        $parenthesesOpened = 0;
        $parenthesesClosed = 0;
        $length = mb_strlen($specification);

        for ($i = 0; $i < $length; ++$i) {
            if ($specification[$i] === '(') {
                ++$parenthesesOpened;
            } elseif ($specification[$i] === ')') {
                ++$parenthesesClosed;
            }

            if ($parenthesesOpened === $parenthesesClosed && $specification[$i] === self::COMPOUND_TYPE_SPLITTER) {
                $parts[] = $this->parse(mb_substr($specification, $start, $i - $start));
                $start = $i + 1;
            }
        }

        $parts[] = $this->parse(mb_substr($specification, $start, $i));

        return new CompoundDocblockType(...$parts);
    }

    /**
     * @param string $specification
     *
     * @return DocblockType
     */
    private function parseNonCompoundTypeSpecification(string $specification): DocblockType
    {
        if ($specification === StringDocblockType::STRING_VALUE) {
            return new StringDocblockType();
        } elseif ($specification === IntDocblockType::STRING_VALUE) {
            return new IntDocblockType();
        } elseif ($specification === IntegerDocblockType::STRING_VALUE) {
            return new IntegerDocblockType();
        } elseif ($specification === BoolDocblockType::STRING_VALUE) {
            return new BoolDocblockType();
        } elseif ($specification === BooleanDocblockType::STRING_VALUE) {
            return new BooleanDocblockType();
        } elseif ($specification === FloatDocblockType::STRING_VALUE) {
            return new FloatDocblockType();
        } elseif ($specification === DoubleDocblockType::STRING_VALUE) {
            return new DoubleDocblockType();
        } elseif ($specification === ObjectDocblockType::STRING_VALUE) {
            return new ObjectDocblockType();
        } elseif ($specification === MixedDocblockType::STRING_VALUE) {
            return new MixedDocblockType();
        } elseif ($specification === ArrayDocblockType::STRING_VALUE) {
            return new ArrayDocblockType();
        } elseif ($specification === ResourceDocblockType::STRING_VALUE) {
            return new ResourceDocblockType();
        } elseif ($specification === VoidDocblockType::STRING_VALUE) {
            return new VoidDocblockType();
        } elseif ($specification === NullDocblockType::STRING_VALUE) {
            return new NullDocblockType();
        } elseif ($specification === CallableDocblockType::STRING_VALUE) {
            return new CallableDocblockType();
        } elseif ($specification === FalseDocblockType::STRING_VALUE) {
            return new FalseDocblockType();
        } elseif ($specification === TrueDocblockType::STRING_VALUE) {
            return new TrueDocblockType();
        } elseif ($specification === SelfDocblockType::STRING_VALUE) {
            return new SelfDocblockType();
        } elseif ($specification === StaticDocblockType::STRING_VALUE) {
            return new StaticDocblockType();
        } elseif ($specification === ThisDocblockType::STRING_VALUE) {
            return new ThisDocblockType();
        } elseif ($specification === IterableDocblockType::STRING_VALUE) {
            return new IterableDocblockType();
        } elseif (preg_match(self::ARRAY_TYPE_HINT_REGEX, $specification, $matches) === 1) {
            $valueType = $this->parse($matches[1]);

            return new SpecializedArrayDocblockType($valueType);
        }

        return new ClassDocblockType($specification);
    }
}
