<?php

namespace Serenata\DocblockTypeParser;

/**
 * Represents a boolean as docblock type.
 *
 * {@inheritDoc}
 */
class BoolDocblockType extends SpecialDocblockType
{
    /**
     * @var string
     */
    public const STRING_VALUE = 'bool';

    /**
     * @inheritDoc
     */
    public function toString(): string
    {
        return self::STRING_VALUE;
    }
}
