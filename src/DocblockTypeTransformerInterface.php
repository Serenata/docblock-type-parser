<?php

namespace Serenata\DocblockTypeParser;

use Closure;

/**
 * Transforms a tree of {@see DocblockType} objects into a tree of new objects by applying a transformation on them.
 */
interface DocblockTypeTransformerInterface
{
    /**
     * @param DocblockType $docblockType
     * @param Closure      $transformer  Closure that should return a new instance of a {@see DocblockType}.
     *
     * @return DocblockType
     */
    public function transform(DocblockType $docblockType, Closure $transformer): DocblockType;
}
