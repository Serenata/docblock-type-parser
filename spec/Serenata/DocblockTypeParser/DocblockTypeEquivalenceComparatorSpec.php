<?php

namespace spec\Serenata\DocblockTypeParser;

use Serenata\DocblockTypeParser;

use PhpSpec\ObjectBehavior;

class DocblockTypeEquivalenceComparatorSpec extends ObjectBehavior
{
    /**
     * @return void
     */
    public function it_deems_special_type_equivalent_of_other_instance_with_same_type(): void
    {
        $a = new DocblockTypeParser\IntDocblockType();
        $b = new DocblockTypeParser\IntDocblockType();

        $this->areEquivalent($a, $b)->shouldBe(true);
    }

    /**
     * @return void
     */
    public function it_deems_special_type_not_equivalent_of_other_instance_with_different_type(): void
    {
        $a = new DocblockTypeParser\IntDocblockType();
        $b = new DocblockTypeParser\StringDocblockType();

        $this->areEquivalent($a, $b)->shouldBe(false);
    }

    /**
     * @return void
     */
    public function it_deems_class_docblock_type_equivalent_of_other_instance_with_same_name(): void
    {
        $a = new DocblockTypeParser\ClassDocblockType('\A');
        $b = new DocblockTypeParser\ClassDocblockType('\A');

        $this->areEquivalent($a, $b)->shouldBe(true);
    }

    /**
     * @return void
     */
    public function it_deems_class_docblock_type_not_equivalent_of_other_instance_with_different_name(): void
    {
        $a = new DocblockTypeParser\ClassDocblockType('\A');

        $b = new DocblockTypeParser\ClassDocblockType('\B');

        $this->areEquivalent($a, $b)->shouldBe(false);
    }

    /**
     * @return void
     */
    public function it_deems_class_docblock_type_not_equivalent_of_other_instance_with_different_type(): void
    {
        $a = new DocblockTypeParser\ClassDocblockType('\A');

        $b = new DocblockTypeParser\NullDocblockType();

        $this->areEquivalent($a, $b)->shouldBe(false);
    }

    /**
     * @return void
     */
    public function it_deems_compound_type_equivalent_of_other_instance_with_equivalent_children(): void
    {
        $a = new DocblockTypeParser\CompoundDocblockType(
            new DocblockTypeParser\IntDocblockType(),
            new DocblockTypeParser\StringDocblockType()
        );

        $b = new DocblockTypeParser\CompoundDocblockType(
            new DocblockTypeParser\IntDocblockType(),
            new DocblockTypeParser\StringDocblockType()
        );

        $this->areEquivalent($a, $b)->shouldBe(true);
    }

    /**
     * @return void
     */
    public function it_deems_compound_type_equivalent_of_other_instance_with_equivalent_children_in_different_order(): void
    {
        $a = new DocblockTypeParser\CompoundDocblockType(
            new DocblockTypeParser\IntDocblockType(),
            new DocblockTypeParser\StringDocblockType()
        );

        $b = new DocblockTypeParser\CompoundDocblockType(
            new DocblockTypeParser\StringDocblockType(),
            new DocblockTypeParser\IntDocblockType()
        );

        $this->areEquivalent($a, $b)->shouldBe(true);
    }

    /**
     * @return void
     */
    public function it_deems_compound_type_not_equivalent_of_other_instance_with_non_equivalent_children(): void
    {
        $a = new DocblockTypeParser\CompoundDocblockType(
            new DocblockTypeParser\IntDocblockType(),
            new DocblockTypeParser\StringDocblockType()
        );

        $b = new DocblockTypeParser\CompoundDocblockType(
            new DocblockTypeParser\NullDocblockType(),
            new DocblockTypeParser\IntDocblockType()
        );

        $this->areEquivalent($a, $b)->shouldBe(false);
    }

    /**
     * @return void
     */
    public function it_deems_compound_type_not_equivalent_of_other_instance_with_more_children(): void
    {
        $a = new DocblockTypeParser\CompoundDocblockType(
            new DocblockTypeParser\IntDocblockType(),
            new DocblockTypeParser\StringDocblockType()
        );

        $b = new DocblockTypeParser\CompoundDocblockType(
            new DocblockTypeParser\IntDocblockType(),
            new DocblockTypeParser\StringDocblockType(),
            new DocblockTypeParser\NullDocblockType()
        );

        $this->areEquivalent($a, $b)->shouldBe(false);
    }

    /**
     * @return void
     */
    public function it_deems_compound_type_not_equivalent_of_other_instance_with_fewer_children(): void
    {
        $a = new DocblockTypeParser\CompoundDocblockType(
            new DocblockTypeParser\IntDocblockType(),
            new DocblockTypeParser\StringDocblockType(),
            new DocblockTypeParser\NullDocblockType()
        );

        $b = new DocblockTypeParser\CompoundDocblockType(
            new DocblockTypeParser\IntDocblockType(),
            new DocblockTypeParser\StringDocblockType()
        );

        $this->areEquivalent($a, $b)->shouldBe(false);
    }

    /**
     * @return void
     */
    public function it_deems_compound_type_not_equivalent_of_other_instance_with_different_type(): void
    {
        $a = new DocblockTypeParser\CompoundDocblockType(
            new DocblockTypeParser\IntDocblockType(),
            new DocblockTypeParser\StringDocblockType()
        );

        $b = new DocblockTypeParser\IntDocblockType();

        $this->areEquivalent($a, $b)->shouldBe(false);
    }
}
